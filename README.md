# Assignment 2 - Agile Software Practice

Name: Xiang Li 20095236

## API endpoints

+ __Authenticate__
  + GET auth/movies/:id - Retrieves details of a specific movie by its ID.
  + GET auth/movies?page=n&limit=m - Fetches a list of movies from TMDB's Discover endpoint, starting at page n with a limit of m movies per page.
  + GET auth/movies/tmdb/upcoming - Accesses upcoming movies data from TMDB.
  +GET auth/movies/tmdb/genres - Retrieves movie genres from TMDB.
+ __Movie API__
  + GET tmdb/movies?page={page} - Fetches a list of movies from TMDB's Discover endpoint, starting at page n with a limit of m movies per page.
  + GET tmdb/movie/:id - Retrieves details of a specific movie by its ID.
  + GET tmdb/movie/:id/images - Retrieves images of a specific movie by its ID.
  + GET tmdb/upcoming - Accesses upcoming movies data from TMDB.
  + GET tmdb/trend - Accesses trend movies data from TMDB.
  + GET tmdb/now_playing - Accesses now_playing movies data from TMDB.
  + GET tmdb/movie/:id/reviews - Retrieves reviews of a specific movie by its ID.
  + GET tmdb/people - Accesses people data from TMDB.
  + GET tmdb/person/:id - Retrieves detail of a specific person by its ID.
  + GET tmdb/movie/:id/credits - Get a movie credit details by ID.
  + GET api/movies/tmdb/genres - Retrieves movie genres from TMDB.
  + GET tmdb/person/:id/movie_credits - Get the movie credits for a person.

+ __User__
  + GET /api/users - Retrieves all registered users.
  + POST /api/users - Depending on the query parameter (`register` or login), registers a new user or authenticates an existing one. It includes password validation.
  + PUT /api/users/:id - Updates a user's information based on their ID.

## Automated Testing

~~~
  Genres endpoint
    GET /api/actors
database connected to test on ac-czmm0nr-shard-00-00.rllhesx.mongodb.net
20 Movies were successfully stored.
4 Genres were successfully stored.
4 Genres were successfully stored.
2 users were successfully stored.
      √ should return 200 status and a list of actors (707ms)

  Users endpoint
    GET /api/users
      √ should return the 2 users and a status 200 (241ms)
    POST /api/users
      For a register action
        when the password does mot meet the requirements
          √ should return a 400 status and the error message
      For an authenticate action
        when the password is incorrect
          √ should return a 401 status and a error message (283ms)
        when the user is unauthenticated
          √ should return a 401 status and a error message (235ms)
    PUT /api/users
      For a update action
        when the id is incorrect
          √ should return a 404 status and the error message (236ms)

  Movies endpoint
    GET /auth/movies without TOKEN
      √ should return 500 status
    GET /auth/movies/:id without TOKEN
      when the id is valid
        √ should return 500 status
      when the id is invalid
        √ should return the NOT found message (466ms)
    GET /auth/movies with TOKEN
      √ should return 20 movies and a status 200 (474ms)
    GET /auth/movies/:id with TOKEN
      when the id is valid
        √ should return the matching movie (463ms)
      when the id is invalid
        √ should return the NOT found message (466ms)
    GET /auth/movies?page:page with TOKEN
      when the page is valid
        √ should return the matching movies form tmdb and status code 200 (472ms)
      when the page is invalid
        √ should return Internal Server Error

  Genres endpoint
    GET /tmdb/genres
      √ should return 200 status and a list of genres (237ms)

  Languages endpoint
    GET /tmdb/languages
      √ should return 200 status and a list of languages (238ms)

  TMDB Movies endpoint
    GET /tmdb/movies
      √ should return 200 status and a list of movies (108ms)
    GET /tmdb/movies with different page
      √ should return 200 status and a list of movies (64ms)
    GET /tmdb/movie/:id
      when the id is valid
        √ should return 200 status and a list of movies
      when the id is invalid
        √ should return Internal Server Error (137ms)
    GET /tmdb/movie/:id/images
      when the id is valid
        √ should return 200 status and a list of images
      when the id is invalid
        √ should return Internal Server Error (138ms)
    GET /tmdb/upcoming
      √ should return 200 status and a list of movies (71ms)
    GET /tmdb/trend
      √ should return 200 status and a list of movies (66ms)
    GET /tmdb/now_playing
      √ should return 200 status and a list of movies (61ms)
    GET /tmdb/movie/:id/reviews
      when the id is valid
        √ should return 200 status and a list of reviews
      when the id is invalid
        √ should return Internal Server Error (151ms)
    GET /tmdb/people
      √ should return 200 status and a list of people (73ms)
    GET /tmdb/person/:id
      when the id is valid
        √ should return 200 status
      when the id is invalid
        √ should return Internal Server Error (139ms)
    GET /tmdb/movie/:id/credits
      when the id is valid
        √ should return 200 status
      when the id is invalid
        √ should return Internal Server Error (130ms)
    GET /tmdb/person/:id/movie_credits
      when the id is valid
        √ should return 200 status
      when the id is invalid
        √ should return Internal Server Error (131ms)


  34 passing (29s)
~~~

## Deployments

Staging: <https://movies-api-staging-xiang-li-fa29097a8733.herokuapp.com/tmdb/movies>
Production: <https://movies-api-production-xiang-li-f54aaac99a53.herokuapp.com/tmdb/movies>

## Independent Learning (if relevant)

+ Swagger API Document
![Alt text](image-1.png)

Coveralls:<https://coveralls.io/gitlab/Onion-L/movies-api-cicd-lab>
[![Coverage Status](https://coveralls.io/repos/gitlab/Onion-L/movies-api-cicd-lab/badge.svg?branch=main)](https://coveralls.io/gitlab/Onion-L/movies-api-cicd-lab?branch=main)
![Alt text](image.png)
